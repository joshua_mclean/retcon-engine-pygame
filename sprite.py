from tile import Tile
from tilemap import TileMap


class Sprite(TileMap):
    MODE_8x8 = 0
    MODE_8x16 = 1

    mode = MODE_8x8

    palette = []
    list = []

    @staticmethod
    def get_size():
        if Sprite.mode == Sprite.MODE_8x8:
            return Tile.size, Tile.size
        return Tile.size, Tile.size * 2

    def __init__(self):
        if Sprite.mode == Sprite.MODE_8x16:
            size = (1, 2)
        else:
            size = (1, 1)
        super().__init__('spr%d' % len(Sprite.list), size, Sprite.palette[0], 0)
        Sprite.list.append(self)
        self.set_tile(0, 0, 0)

    # GB sprites are offset by -8, -16 b/c top-left of LCF is (8, 16)
    def get_pos(self):
        return (self.x - 8) % 256, (self.y - 16) % 256

    def set_pos(self, x, y):
        self.x = x
        self.y = y

    def set_spr_palette(self, spr_palette_id):
        palette = Sprite.palette[spr_palette_id]
        super().set_palette(palette)

    def set_spr_tile(self, tile_id):
        # TODO check valid tile IDs
        super().set_tile(0, 0, tile_id)
        if Sprite.mode == Sprite.MODE_8x16:
            super().set_tile(0, 1, tile_id + 1)

