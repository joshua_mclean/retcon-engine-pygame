import random

import pygame
from pygame import Surface

import editor
from tilemap import TileMap, Background
from sprite import Sprite
from palette import Palette
from tile import Tile


class Engine:
    def __init__(self, name, resolution, tile_size, window_scale):
        self.update = []
        self.name = name
        self.game_resolution = resolution
        Tile.size = tile_size

        # reset memory
        Tile.def_list = []
        Background.list = []
        Sprite.list = []

        print("Initializing Pygame")
        pygame.init()
        pygame.display.set_caption("Retcon Engine (%s)" % self.name)
        print("Creating virtual screen")
        self.virtual_screen = Surface(self.game_resolution, 0, 8)
        print("Creating window")
        self.window_scale = window_scale
        self.window_resolution = tuple([window_scale * x for x in self.game_resolution])
        self.screen = pygame.display.set_mode(self.window_resolution, 0, 8)

    def __str__(self):
        return "%s: %s, %d colors, %d backgrounds, %d sprites" % (self.name, self.game_resolution,
                                                                  len(Palette.palette_list),
                                                                  len(Background.list),
                                                                  len(Sprite.list))

    def link_to_editor(self):
        self.allow_edit = True

    def run(self, run_time_sec=0):
        print("Running engine: %s" % self)
        clock = pygame.time.Clock()
        running = True
        start_time = prev_time = pygame.time.get_ticks()
        while running:

            # run update functions
            # if they return False, remove them from the list
            to_remove = []
            for func in self.update:
                if not func():
                    to_remove.append(func)
            for func in to_remove:
                self.update.remove(func)

            self._render()
            for event in pygame.event.get():
                # keypress
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_F2:
                        editor.init()
                #mouse
                if event.type == pygame.MOUSEBUTTONDOWN:
                    self._on_click(*pygame.mouse.get_pos())
                # quit
                if (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or event.type == pygame.QUIT:
                    running = False

            clock.tick(60)

            # display time remaining for test
            if run_time_sec > 0:
                time_left = run_time_sec - (prev_time - start_time) / 1000
                pygame.display.set_caption("Retcon Engine (%s) TESTING %0.2fs" % (self.name, time_left))
                if time_left < 0:
                    running = False
        self.stop()

    def stop(self):
        pygame.display.quit()
        pygame.quit()

    def _clear_screen(self):
        self.virtual_screen.fill(2)
        self.screen.fill(0)

    def _draw_screen(self):
        scaled_screen = pygame.transform.scale(self.virtual_screen, self.window_resolution)
        self.screen.blit(scaled_screen, (0, 0))
        pygame.display.flip()

    def _on_click(self, x, y):
        if not self.allow_edit:
            return
        tile_size = Tile.size * self.window_scale
        tile_x = int(x / tile_size)
        tile_y = int(y / tile_size)
        print("Click (%d, %d)" % (tile_x, tile_y))
        if not 0 <= tile_x < Tile.size or not 0 <= tile_y < Tile.size:
            return

        edit_tile = Tile.def_list[editor.tile_index]
        color = edit_tile.get_pixel_at(tile_x, tile_y)

        buttons = pygame.mouse.get_pressed()
        if buttons[0]:
            color -= 1
        else:
            color += 1
        color %= len(self.bg.get_palette())
        edit_tile.set_pixel_at(tile_x, tile_y, color)

    def _render(self):
        self._clear_screen()
        for background in Background.list:
            if not background.visible:
                continue

            main_offset = background.get_offset()
            main_width = self.game_resolution[0] - main_offset[0]
            main_height = self.game_resolution[1] - main_offset[1]
            main_rect = pygame.Rect((0, 0), (main_width, main_height))
            self.virtual_screen.blit(background.surface, main_offset, main_rect)

            if not background.loop:
                continue

            # left
            if main_offset[0] > 0:
                left_offset = (0, main_offset[1])
                left_width = main_offset[0]
                left_height = main_height
                left_start = (background.width - left_width, 0)
                left_rect = pygame.Rect(left_start, (left_width, left_height))
                self.virtual_screen.blit(background.surface, left_offset, left_rect)

            # top
            if main_offset[1] > 0:
                top_offset = (main_offset[0], 0)
                top_width = main_width
                top_height = main_offset[1]
                top_start = (0, background.height - top_height)
                top_rect = pygame.Rect(top_start, (top_width, top_height))
                self.virtual_screen.blit(background.surface, top_offset, top_rect)

            if main_offset[0] > 0 and main_offset[1] > 0:
                tl_start = (background.width - main_offset[0], background.height - main_offset[1])
                tl_rect = pygame.Rect(tl_start, main_offset)
                self.virtual_screen.blit(background.surface, (0, 0), tl_rect)

        for sprite in Sprite.list:
            if not sprite.visible:
                continue
            self.virtual_screen.blit(sprite.surface, sprite.get_pos())

        self._draw_screen()


class GameBoy(Engine):
    def __init__(self, window_scale=6):
        GameBoy._init_palettes()
        name = "Game Boy"
        game_resolution = (160, 144)
        tile_size = 8
        super().__init__(name, game_resolution, tile_size, window_scale)
        print("Initializing tile memory")
        for count in range(256):
            Tile.add()
        Tile.randomize_all()
        self.bg, self.win = GameBoy._init_backgrounds()
        Sprite.size = (1, 2)
        GameBoy._init_sprites()

    @staticmethod
    def move_sprites():
        multiplier = 8
        minimum = 0.4
        for sprite in Sprite.list:
            x = random.random()
            if x > minimum:
                sprite.x += multiplier * (x - 0.5)
            y = random.random()
            if y > minimum:
                sprite.y += multiplier * (y - 0.5)

    @staticmethod
    def _init_backgrounds():
        print("Initializing background")
        bg = Background('bg', (32, 32))
        bg.loop = True
        bg.randomize()
        bg.x = random.randint(0, 255)
        bg.y = random.randint(0, 255)

        print("Initializing window")
        win = Background('win', (20, 18))
        win.randomize()
        win.visible = False

        return bg, win

    '''
    Initialize Game Boy palettes. All palettes are identical for backgrounds.
    
    We always use color #0 as transparent, so for sprites, the visible colors are the three after the first.
    
    0: dark gray, light gray, white
    1: light gray, white, black
    2: white, black, dark gray
    3: black, dark gray, light gray
    
    Palettes 4 - 7 are inversions of 0 - 3.
    
    4: white, light gray, dark gray
    5: black, white, light gray
    6: dark gray, black, white
    7: light gray, dark gray, black
    '''
    @staticmethod
    def _init_palettes():
        black = (0, 0, 0)
        dark_gray = (84, 84, 84)
        light_gray = (169, 169, 169)
        white = (255, 255, 255)

        Palette.add([black, dark_gray, light_gray, white])
        Palette.add([dark_gray, light_gray, white, black])
        Palette.add([light_gray, white, black, dark_gray])
        Palette.add([white, black, dark_gray, light_gray])

        Palette.add([white, light_gray, dark_gray, black])
        Palette.add([light_gray, dark_gray, black, white])
        Palette.add([dark_gray, black, white, light_gray])
        Palette.add([black, white, light_gray, dark_gray])

        Background.palette = 0
        Sprite.palette = [1, 2]

    @staticmethod
    def _init_sprites():
        print("Initializing 40 sprites")
        for i in range(0, 40):
            sprite = Sprite()
            sprite.set_spr_palette(random.randint(0, 1))
            sprite.randomize()
            sprite.x = random.randint(0, 255)
            sprite.y = random.randint(0, 255)
            sprite.visible = False

