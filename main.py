import random
import time

import pygame

import editor
from character import Character
from engine import GameBoy
from sprite import Sprite


EDITOR_ENABLED = True


def main():
    random.seed = time.time()

    game_boy = GameBoy()
    game_boy.update.append(editor.update)

    if EDITOR_ENABLED:
        editor.init(game_boy)
        game_boy.link_to_editor()
    game_boy.run()

    if EDITOR_ENABLED:
        editor.close()


def run_tests():
    test_length = 5
    test_tall_sprite(test_length)
    test_load_player(test_length)
    test_player(test_length)
    test_img(test_length)


def test_tall_sprite(test_length):
    Sprite.mode = Sprite.MODE_8x16
    game_boy = GameBoy()
    game_boy.bg.visible = False
    Sprite.list[0].visible = True
    Sprite.list[0].set_pos(20, 20)
    game_boy.run(test_length)


def test_img(test_length):
    game_boy = GameBoy()
    game_boy.bg.visible = False
    img = pygame.image.load('test.png')
    img_gb = img.convert(8)
    width = img_gb.get_width()
    height = img_gb.get_height()
    print("Loaded %dx%d image" % (width, height))
    for y in range(height):
        for x in range(width):
            pixel = img_gb.get_at((x, y))
            color = img_gb.map_rgb(pixel)
            print("%d " % color, end='')
        print()
    game_boy.run(test_length)


def test_load_player(test_length):
    game_boy = GameBoy()
    player = Character.from_img('test.png')
    player.set_visible(True)
    game_boy.bg.visible = False

    def update():
        x = player.x + 1
        y = player.y + 1
        player.set_pos(x, y)

    game_boy.update.append(update)
    game_boy.run(test_length)


def test_player(test_length):
    game_boy = GameBoy()
    player = Character((2, 2))
    player.set_visible(True)

    game_boy.bg.visible = False

    def update():
        x = player.x + 1
        y = player.y + 1
        player.set_pos(x, y)

    game_boy.update.append(update)
    game_boy.run(test_length)


if __name__ == '__main__':
    main()

