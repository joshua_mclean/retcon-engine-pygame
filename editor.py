import tkinter as tk

import utility

from tile import Tile

frame_tile = None
frame_pixel = None
frame_position = None
engine = None
root = None

pixel_x = 0
pixel_y = 0
tile_index = 0

lbl_position = None
lbl_pixel_index = None
lbl_tile_index = None

sb_x = None
sb_y = None

set_all = None
show_all = None
show_tiled = None
scroll_x = None
scroll_y = None

tile_blank = Tile()


def init(target_engine):
    if root is not None:
        print("Open editor FAILED - root already exists")
        return
    print("Open editor")

    global engine
    engine = target_engine

    init_tk()
    init_frames()
    init_buttons()

    update_tile()


def init_buttons():
    global lbl_tile_index
    lbl_tile_index = tk.Label(frame_tile)
    lbl_tile_index.pack()

    btn_tile_prev = tk.Button(frame_tile, text="Prev Tile (RAM)", command=prev_tile)
    btn_tile_prev.pack(side=tk.LEFT)
    btn_tile_next = tk.Button(frame_tile, text="Next Tile (RAM)", command=next_tile)
    btn_tile_next.pack(side=tk.RIGHT)

    init_frame_position()


def init_frame_position():
    global lbl_position
    lbl_position = tk.Label(frame_position)
    lbl_position.pack()

    global sb_x
    frame_x = tk.Frame(frame_position)
    sb_x = tk.Scale(frame_x, from_=0, to=255, orient=tk.HORIZONTAL)
    sb_x.pack()
    tk.Button(frame_x, text="-", command=decrease_x).pack(side=tk.LEFT)
    tk.Button(frame_x, text="+", command=increase_x).pack(side=tk.RIGHT)
    frame_x.pack()

    global sb_y
    frame_y = tk.Frame(frame_position)
    sb_y = tk.Scale(frame_y, from_=0, to=255)
    sb_y.pack()
    tk.Button(frame_y, text="-", command=decrease_y).pack(side=tk.LEFT)
    tk.Button(frame_y, text="+", command=increase_y).pack(side=tk.RIGHT)
    frame_y.pack()


def decrease_x():
    global sb_x
    sb_x.set(sb_x.get() - 1)


def increase_x():
    global sb_x
    sb_x.set(sb_x.get() + 1)


def decrease_y():
    global sb_y
    sb_y.set(sb_y.get() - 1)


def increase_y():
    global sb_y
    sb_y.set(sb_y.get() + 1)


def init_frames():
    tk.Button(root, text="Randomize Tiles", command=Tile.randomize_all).pack()
    tk.Button(root, text="Color Blocks", command=Tile.color_blocks).pack()

    global show_all
    show_all = tk.BooleanVar(root)
    show_all.set(0)
    tk.Checkbutton(root, text="Show All", variable=show_all).pack()

    global show_tiled
    show_tiled = tk.BooleanVar(root)
    show_tiled.set(0)
    tk.Checkbutton(root, text="Show Tiled", variable=show_tiled).pack()

    global scroll_x
    scroll_x = tk.BooleanVar(root)
    scroll_x.set(0)
    tk.Checkbutton(root, text="Scroll X", variable=scroll_x).pack()

    global scroll_y
    scroll_y = tk.BooleanVar(root)
    scroll_y.set(0)
    tk.Checkbutton(root, text="Scroll Y", variable=scroll_y).pack()

    global set_all
    set_all = tk.BooleanVar(root)
    set_all.set(0)

    global frame_tile, frame_position
    frame_tile = tk.Frame(root)
    frame_tile.pack()
    frame_position = tk.Frame(root)
    frame_position.pack()


def init_tk():
    global root
    root = tk.Tk()
    root.protocol("WM_DELETE_WINDOW", close)


def close():
    global root
    print("Close editor")
    if root is None:
        return

    global frame_tile
    frame_tile.destroy()
    frame_tile = None

    root.destroy()
    root = None


def prev_tile():
    global tile_index
    tile_index -= 1
    if tile_index < 0:
        tile_index = len(Tile.def_list) - 1
    update_tile()


def next_tile():
    global tile_index
    tile_index += 1
    if tile_index >= len(Tile.def_list):
        tile_index = 0
    update_tile()


def update():
    if root is None:
        return False
    try:
        frame_tile.update()

        if scroll_x.get() == 1:
            if sb_x.get() == 255:
                sb_x.set(0)
            else:
                sb_x.set(sb_x.get() + 1)
        engine.bg.x = sb_x.get()

        if scroll_y.get() == 1:
            if sb_y.get() == 255:
                sb_y.set(0)
            else:
                sb_y.set(sb_y.get() + 1)
        engine.bg.y = sb_y.get()

        update_tile()
    except tk.TclError as ex:
        print("Error in editor: %s" % ex)
    return True


def update_tile():
    lbl_tile_index.config(text="Tile: 0x%X" % tile_index)
    if show_all.get() == 1:
        for i in range(len(Tile.def_list)):
            x, y = utility.addr_to_coords(i, engine.bg.width_tiles)
            engine.bg.set_tile(x, y, i)
        while x < engine.bg.width_tiles:
            while y < engine.bg.width_tiles:
                engine.bg.set_tile(x, y, 0)
                y += 1
            x += 1
    else:
        if show_tiled.get() == 1:
            engine.bg.set_all(tile_index)
        else:
            engine.bg.clear()
            for x in range(Tile.size):
                for y in range(Tile.size):
                    engine.bg.set_color_at(x, y, Tile.def_list[tile_index].get_pixel_at(x, y))

