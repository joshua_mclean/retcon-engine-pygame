import utility

from palette import Palette


class Tile:
    def_list = []
    size = 8

    @staticmethod
    def add():
        tile = Tile()
        Tile.def_list.append(tile)
        return len(Tile.def_list) - 1, tile

    @staticmethod
    def color_blocks():
        for i in range(len(Tile.def_list)):
            for x in range(Tile.size):
                for y in range(Tile.size):
                    Tile.def_list[i].pixel[y][x] = i % len(Palette.palette_list[0])

    @staticmethod
    def randomize_all():
        for i in range(len(Tile.def_list)):
            Tile.def_list[i].randomize()

    def __init__(self):
        self.pixel = [[0 for x in range(Tile.size)] for y in range(Tile.size)]
        self.clear()

    def clear(self):
        self.set_all(0)

    def set_all(self, color):
        for x in range(Tile.size):
            for y in range(Tile.size):
                self.pixel[y][x] = color

    def get_pixel(self, i):
        (x, y) = utility.addr_to_coords(i, Tile.size)
        return self.pixel[y][x]

    def get_pixel_at(self, x, y):
        return self.pixel[y][x]

    def load(self, surface, x, y):
        for y in range(0, Tile.size):
            for x in range(0, Tile.size):
                color = surface.get_at((x, y))
                self.pixel[y][x] = surface.map_rgb(color)

    def print(self):
        for y in range(0, Tile.size):
            for x in range(0, Tile.size):
                print("%2d" % self.pixel[y][x], end='')
            print()

    def randomize(self):
        for x in range(0, Tile.size):
            for y in range(0, Tile.size):
                self.pixel[y][x] = Palette.random_color()

    def set_pixel(self, i, value):
        (x, y) = utility.addr_to_coords(i, Tile.size)
        x %= Tile.size
        y %= Tile.size
        self.pixel[y][x] = value

    def set_pixel_at(self, x, y, value):
        x %= Tile.size
        y %= Tile.size
        self.pixel[y][x] = value

