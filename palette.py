from random import randint


class Palette:
    palette_list = []

    @staticmethod
    def add(palette):
        Palette.palette_list.append(palette)

    @staticmethod
    def random_color():
        return randint(0, len(Palette.palette_list[0]) - 1)
