from pygame import Surface

class Memory:

    # must be initialized AFTER pygame
    @staticmethod
    def init(name, game_resolution):
        Memory.name = name
        Memory.game_resolution = game_resolution
        Memory.virtual_screen = Surface(Memory.game_resolution, 0, 8)
