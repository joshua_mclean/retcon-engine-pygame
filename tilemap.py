import random

from pygame import Surface

from palette import Palette
from tile import Tile


class TileMap:
    def __init__(self, name, tiles_xy, palette_id=0, clear_color=None):
        self.x = self.y = 0
        self.name = name
        self.visible = True
        self.loop = False
        self.width_tiles = tiles_xy[0]
        self.height_tiles = tiles_xy[1]
        self.map = [[0 for x in range(self.width_tiles)] for y in range(self.height_tiles)]

        self.width = self.width_tiles * Tile.size
        self.height = self.height_tiles * Tile.size
        self.surface = Surface((self.width, self.height), 0, 8)
        self.clear_color = clear_color
        self.palette_id = palette_id
        self.set_palette(self.palette_id)

    def clear(self):
        for x in range(self.width):
            for y in range(self.height):
                self.surface.set_at((x, y), 0)

    def get_offset(self):
        if self.loop:
            return self.x % self.width, self.y % self.height
        return self.x, self.y

    def get_palette(self):
        return Palette.palette_list[self.palette_id]

    def randomize(self):
        for x in range(self.width_tiles):
            for y in range(self.height_tiles):
                self.set_tile(x, y, random.randint(0, 255))

    def set_all(self, tile_id):
        for y in range(self.width_tiles):
            for x in range(self.height_tiles):
                self.set_tile(x, y, tile_id)

    def set_color_at(self, x, y, color):
        ox = x * Tile.size
        oy = y * Tile.size
        for x in range(Tile.size):
            for y in range(Tile.size):
                self.surface.set_at((ox + x, oy + y), color)

    def set_palette(self, palette_id):
        palette = Palette.palette_list[palette_id]
        self.surface.set_palette(palette)
        if self.clear_color is not None:
            self.surface.set_colorkey(self.clear_color)

    def set_tile(self, tx, ty, tile_id):
        self.map[ty][tx] = tile_id
        tile = Tile.def_list[tile_id]
        self.surface.lock()
        for px in range(tile.size):
            for py in range(tile.size):
                x = tx * Tile.size + px
                y = ty * Tile.size + py
                self.surface.set_at((x, y), tile.get_pixel_at(px, py))
        self.surface.unlock()


class Background(TileMap):
    list = []
    palette = 0

    def __init__(self, name, tiles_xy):
        super().__init__(name, tiles_xy, Background.palette)
        Background.list.append(self)

