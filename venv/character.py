import pygame

from sprite import Sprite
from tile import Tile


class Character:
    next_index = 0

    @staticmethod
    def from_img(img_path):
        img = pygame.image.load('test.png').convert(8)
        width = img.get_width()
        height = img.get_height()
        width_tiles = int(width / Tile.size)
        height_tiles = int(height / Tile.size)
        char = Character((width_tiles, height_tiles))

        tile_id = 0
        for ty in range(height_tiles):
            for tx in range(width_tiles):
                Tile.def_list[tile_id].load(img, tx * Tile.size, ty * Tile.size)
                char.sprite[ty][tx] = Sprite.list[tile_id]
                char.sprite[ty][tx].set_spr_tile(tile_id)
                tile_id += 1

        return char

    def __init__(self, size):
        self.x = self.y = 0
        self.width = size[0]
        self.height = size[1]
        self.sprite = [[0 for x in range(self.width)] for y in range(self.height)]
        for x in range(self.width):
            for y in range(self.height):
                self.sprite[y][x] = Sprite.list[Character.next_index]
                Character.next_index += 1
                # TODO make sure we don't go over sprite limit

    def set_pos(self, x, y):
        self.x = x
        self.y = y
        for sx in range(self.width):
            for sy in range(self.height):
                self.sprite[sy][sx].x = self.x + sx * Tile.size
                self.sprite[sy][sx].y = self.y + sy * Tile.size

    def set_visible(self, visible):
        for sx in range(self.width):
            for sy in range(self.height):
                self.sprite[sy][sx].visible = visible
